// Arrays
// used to store multiple values in a single variable
// array=[ ]

// let students = ['Darwin', 'Rancel', 'Yong', 'Kaka']

// let randomThings = [23,'Albert', true];
// console.log(randomThings);
// console.log(students[3])

// console.log(students[0].length);
// let password="iloveyou";
// console.log(password.length);
// if(password.length<9){
// 	console.log("Invalid Password");
// }

// index

// add a value to an array
// .push

// students.push("Krystel");


let students = ['Darwin', 'Rancel', 'Yong', 'Kaka'];
console.log(students);
// To remove the last value in an array and return the new array;
// .pop()

// to remove an element based on its index;
// and to add elements using the indez as its position
// .splice(index, # of element we want to remove, element we want to add)

// Iteration Method
// .forEach

// students.forEach(function(student, index){
// 	console.log("Contestant #" + index+1  + ": " +student + " Pogi");
// });


students.forEach(function(pikachu,index){
	if(index===0|| index ===3){
		console.log("Contestant #" + index+1  + ": " + pikachu + " Pogi");
	}else{
		console.log("Thank you for joining " + pikachu);
	}
});